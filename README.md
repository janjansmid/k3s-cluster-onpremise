# K3S cluster on premise using Ansible

## Prerequisities
You need 4 servers with atleast one user account with sudo privileges accessible through SSH, you can create them using baremetal or in Proxmox, VirtualBox, VmWare, ... whatever you like.

### Add user 'ansible' and your SSH public key to servers
1) Edit add-user-ssh-key-play.yml, set *remote_user* accordingly to your setup.
2) Set *remote_user* for your localhost in
  - roles/k3s-workers/tasks/delete-node-token.yml
  - roles/get-kubeconfig/tasks/get-kubeconfig.yml.
```bash
ansible-playbook -i hosts.ini add-user-ssh-key-play.yml --ask-pass 
``` 
2) *--ask-pass* will prompt you for your remote_user's password  
3) All other playbooks are using *remote_user: ansible*
4) *--ask-become-pass* will prompt you for password you set using *add-user-ssh-key-play* playbook.

### Edit hosts file
Edit IP addresses in hosts.ini accordingly to your setup.

## Setup K3S cluster automatically
- Initiate ssh agent, delete old cluster and setup new one.
- It can take some time to load depending on your resources, mainly Rancher and Longhorn are quite demanding.
```bash
ssh-add
ansible-playbook -i hosts.ini k3s-setup-all.yml --ask-become-pass
``` 
## Managing K3S cluster "manually"

1) Initiate ssh agent
```bash
ssh-add
``` 
2) Delete k3s cluster
```bash
ansible-playbook -i hosts.ini k3s-delete-cluster-play.yml --ask-become-pass
``` 
3) Setup k3s cluster
```bash
ansible-playbook -i hosts.ini k3s-setup-cluster-play.yml --ask-become-pass
``` 
4) Shutdown servers
```bash
ansible-playbook -i hosts.ini srv-shutdown-play.yml --ask-become-pass
``` 
5) Reboot servers
```bash
ansible-playbook -i hosts.ini srv-reboot-play.yml --ask-become-pass
``` 
6) Deploy helm charts
```bash
ansible-playbook -i hosts.ini k3s-helm-charts-play.yml --ask-become-pass
```
## Post cluster setup steps

### Setup local DNS
- Update your local DNS server to serve used ingress URLs and point them towards your k3s master server, or setup round-robin DNS.
- Ingresses are defined in *roles/k3s-helm/files* directory.
- This project uses following ingress URLs, but you can change them as you like:
  - https://longhorn.k3s.local
  - https://grafana.k3s.local
  - https://portainer.k3s.local
  - https://argocd.k3s.local
  - https://rancher.k3s.local
### Restart crashed deployment
- Monitoring-grafana usually ends in CrashLoopBackoff, the same after cluster reboot.
```bash
kubectl rollout restart deployment monitoring-grafana -n monitoring
```
### Get argocd initial password 
```bash
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d && echo  
```
### Import Grafana dashboards
- [LokiForLogs](https://grafana.com/grafana/dashboards/15587-lokiforlogs/)
- [K3S monitoring](https://grafana.com/grafana/dashboards/15282-k8s-rke-cluster-monitoring/)
- Grafana default credentials are
  - user: admin 
  - pass: prom-operator
  - Add data source 'Loki', its service is available at http://loki:3100 .

### Edit kube config
- <span style="color:red">This is just for reference, all these steps are performed by the get-kubeconfig ansible role.</span>
- Remove deleted cluster/context/user from kubeconfig and join it with new one
```bash
kubectl config delete-cluster default
kubectl config delete-context default
kubectl config delete-user default

KUBECONFIG=~/.kube/config:~/.kube/config-k3s kubectl config view --flatten > /tmp/config
mv ~/.kube/config ~/.kube/config.old #backup old config
mv /tmp/config ~/.kube/config
chmod 700 ~/.kube/config

# Change cluster IP in new config
sed -in 's/127.0.0.1/YOUR_K3S_MASTER_IP/g' ~/.kube/config
```

<p align="center">
  <a href="images/rancher.png"><img src="./images/rancher.png" width="45%"></a> &nbsp;
  <a href="images/longhorn.png"><img src="./images/longhorn.png" width="45%"></a> &nbsp;
  <a href="images/grafana.png"><img src="./images/grafana.png" width="45%"></a> &nbsp;
  <a href="images/loki.png"><img src="./images/loki.png" width="45%"></a> &nbsp;
  <a href="images/loki-data-source.png"><img src="./images/loki-data-source.png" height="45%" width="45%"></a> &nbsp;
  <a href="images/argo.png"><img src="./images/argo.png" height="45%" width="45%"></a>
</p>