echo '192.168.210.220 hello.k3s.local' | sudo tee -a /etc/hosts
echo '192.168.210.220 retail.k3s.local' | sudo tee -a /etc/hosts
echo '192.168.210.220 portainer.k3s.local' | sudo tee -a /etc/hosts
echo '192.168.210.220 grafana.k3s.local' | sudo tee -a /etc/hosts
echo '192.168.210.220 argocd.k3s.local' | sudo tee -a /etc/hosts
kubectl patch storageclass local-path -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'

##################################################
helm repo add portainer https://portainer.github.io/k8s/
helm repo update
helm upgrade --install --create-namespace -n portainer portainer portainer/portainer \
    --set service.type=ClusterIP \
    --set persistence.storageClass=nfs-client \
    --set ingress.enabled=true \
    --set ingress.ingressClassName=traefik \
    --set ingress.annotations."nginx\.ingress\.kubernetes\.io/backend-protocol"=HTTPS \
    --set ingress.hosts[0].host=portainer.k3s.local \
    --set ingress.hosts[0].paths[0].path="/"
    # --set tls.force=true \

kubectl apply -f ../ansible/roles/k3s-helm/files/ingress-portainer.yaml

################################################
helm uninstall portainer

kubectl delete all --all -n portainer
kubectl delete clusterrolebindings.rbac.authorization.k8s.io portainer 
kubectl delete serviceaccounts portainer-sa-clusteradmin
kubectl delete pvc portainer 

################################################
### https://www.jeffgeerling.com/blog/2022/quick-hello-world-http-deployment-testing-k3s-and-traefik
kubectl create configmap hello-world --from-file index.html
kubectl apply -f deploy-hello-world.yml

#################################################
kubectl create namespace mailhog
kn mailhog
helm repo add codecentric https://codecentric.github.io/helm-charts
helm upgrade --install mailhog codecentric/mailhog --namespace mailhog --create-namespace
# Web UI:
# =======
# export POD_NAME=$(kubectl get pods --namespace mailhog -l "app.kubernetes.io/name=mailhog,app.kubernetes.io/instance=mailhog" -o jsonpath="{.items[0].metadata.name}")
# kubectl port-forward --namespace mailhog $POD_NAME 8025

# SMTP Server:
# ============
# export POD_NAME=$(kubectl get pods --namespace mailhog -l "app.kubernetes.io/name=mailhog,app.kubernetes.io/instance=mailhog" -o jsonpath="{.items[0].metadata.name}")
# kubectl port-forward --namespace mailhog $POD_NAME 1025

#################################################
# 
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm upgrade --install monitoring prometheus-community/kube-prometheus-stack --namespace monitoring --create-namespace #--version 13.4.1 #--values kube-prometheus-stack-values.yaml
kubectl apply -f ../ansible/roles/k3s-helm/files/ingress-grafana.yaml


#################################################
# https://k3s.rocks/logging/
helm repo add grafana https://grafana.github.io/helm-charts && \
helm repo update && \
helm upgrade --install loki grafana/loki-stack  --namespace monitoring --create-namespace

#################################################
# https://k3s.rocks/argocd/
kubectl create namespace argocd
kn argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml  
kubectl patch deploy argocd-server -n argocd --type json -p '[{"op": "replace", "path": "/spec/template/spec/containers/0/command", "value": ["argocd-server", "--insecure", "--staticassets","/shared/app"]}]'  
kubectl apply -f ../ansible/roles/k3s-helm/files/ingress-argocd.yaml
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d && echo  


kubectl rollout restart deployment <deployment_name> -n <namespace>
